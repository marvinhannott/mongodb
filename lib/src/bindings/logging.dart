import 'libmongoc.dart';
import 'dart:ffi';

typedef MongoCLogFunc = Void Function(Int32 logLevel, Pointer<Uint8> logDomain,
    Pointer<Uint8> message, Pointer<Void> userData);

final setHandler = libmongoc.lookupFunction<
    Void Function(
        Pointer<NativeFunction<MongoCLogFunc>>, Pointer<Void> userData),
    void Function(Pointer<NativeFunction<MongoCLogFunc>>,
        Pointer<Void> userData)>('mongoc_log_set_handler');

final log = libmongoc.lookupFunction<
    Void Function(Int32 logLevel, Pointer<Uint8> domain, Pointer<Uint8> format),
    void Function(int logLevel, Pointer<Uint8> domain,
        Pointer<Uint8> format)>('mongoc_log');

final logLevelString = libmongoc.lookupFunction<
    Pointer<Uint8> Function(Int32 logLevel),
    Pointer<Uint8> Function(int logLevel)>('mongoc_log_level_str');
final defaultHandler = libmongoc.lookupFunction<
    Void Function(Int32 logLevel, Pointer<Uint8> domain, Pointer<Uint8> message,
        Pointer<Void> userData),
    void Function(int logLevel, Pointer<Uint8> domain, Pointer<Uint8> message,
        Pointer<Void> userData)>('mongoc_log_default_handler');

final traceEnable = libmongoc
    .lookupFunction<Void Function(), void Function()>('mongoc_trace_enable');
final traceDisable = libmongoc
    .lookupFunction<Void Function(), void Function()>('mongoc_trace_disable');
