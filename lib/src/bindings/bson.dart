import 'dart:ffi';

class Bson extends Struct {
  @Uint32()
  int flags;
  @Uint32()
  int len;

  /// 120 bytes
  Pointer<Uint8> padding;
}
