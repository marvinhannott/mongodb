import 'dart:ffi';

class BsonError extends Struct {
  @Uint32()
  int domain;
  @Uint32()
  int code;

  /// 504 bytes
  Pointer<Uint8> message;
}
