import 'dart:ffi';

final libmongoc = DynamicLibrary.open('libmongoc');

final init =
    libmongoc.lookupFunction<Void Function(), void Function()>('mongoc_init');
final clealup = libmongoc
    .lookupFunction<Void Function(), void Function()>('mongoc_cleanup');
