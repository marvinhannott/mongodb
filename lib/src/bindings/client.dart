import 'libmongoc.dart';
import 'dart:ffi';

/// returns mongo_client
final clientNew = libmongoc.lookupFunction<
    Pointer<Void> Function(Pointer<Uint8> uri),
    Pointer<Void> Function(Pointer<Uint8> uri)>('mongoc_client_new');

final clientDestroy = libmongoc.lookupFunction<
    Void Function(Pointer<Void> client),
    void Function(Pointer<Void> client)>('mongoc_client_destroy');

final getDatabase = libmongoc.lookupFunction<
    Pointer<Void> Function(Pointer<Void> client),
    Pointer<Void> Function(Pointer<Void> client)>('mongoc_client_get_database');
