import 'dart:ffi';
import 'bson.dart';
import 'errors.dart';
import 'libmongoc.dart';

typedef MongoCAddUser = Int8 Function(
    Pointer<Void> db,
    Pointer<Uint8> userName,
    Pointer<Uint8> password,
    Pointer<Bson> roles,
    Pointer<Bson> customData,
    Pointer<BsonError> error);
typedef DartAdduser = int Function(
    Pointer<Void> db,
    Pointer<Uint8> userName,
    Pointer<Uint8> password,
    Pointer<Bson> roles,
    Pointer<Bson> customData,
    Pointer<BsonError> error);

/// [db] is type mongoc_database_t
/// returns boolean
final addUser = libmongoc
    .lookupFunction<MongoCAddUser, DartAdduser>('mongoc_database_add_user');

typedef MongoCAggregate = Pointer<Void> Function(
    Pointer<Void> db,
    Pointer<Bson> pipeline,
    Pointer<Bson> options,
    Pointer<Void> readPreferences);
typedef DartAggregate = Pointer<Void> Function(
    Pointer<Void> db,
    Pointer<Bson> pipeline,
    Pointer<Bson> options,
    Pointer<Void> readPreferences);

/// [readPreferences] is type mongoc_red_prefs_t
/// returns cursor
final aggregate = libmongoc.lookupFunction<MongoCAggregate, DartAggregate>(
    'mongoc_database_aggregate');

typedef MongoCCommandWithOptions = Int8 Function(
    Pointer<Void> db,
    Pointer<Bson> command,
    Pointer<Void> readPrefs,
    Pointer<Bson> options,
    Pointer<Bson> reply,
    Pointer<BsonError> error);

typedef DartCommandWithOptions = int Function(
    Pointer<Void> db,
    Pointer<Bson> command,
    Pointer<Void> readPrefs,
    Pointer<Bson> options,
    Pointer<Bson> reply,
    Pointer<BsonError> error);

/// [readPrefs] is type mongoc_red_prefs_t
/// returns bool
final commandWithOptions =
    libmongoc.lookupFunction<MongoCCommandWithOptions, DartCommandWithOptions>(
        'mongoc_database_command_with_opts');

typedef MongoCReadCommandWithOptions = Int8 Function(
    Pointer<Void> db,
    Pointer<Bson> command,
    Pointer<Void> readPrefs,
    Pointer<Bson> options,
    Pointer<Bson> reply,
    Pointer<BsonError> error);

typedef DartReadCommandWithOptions = int Function(
    Pointer<Void> db,
    Pointer<Bson> command,
    Pointer<Void> readPrefs,
    Pointer<Bson> options,
    Pointer<Bson> reply,
    Pointer<BsonError> error);

/// [readPrefs] is type mongoc_red_prefs_t
/// returns bool
final readCommandWithOptions = libmongoc.lookupFunction<
    MongoCReadCommandWithOptions,
    DartReadCommandWithOptions>('mongoc_database_read_command_with_opts');

typedef MongoCWriteCommandWithOptions = Int8 Function(
    Pointer<Void> db,
    Pointer<Bson> command,
    Pointer<Bson> options,
    Pointer<Bson> reply,
    Pointer<BsonError> error);

typedef DartWriteCommandWithOptions = int Function(
    Pointer<Void> db,
    Pointer<Bson> command,
    Pointer<Bson> options,
    Pointer<Bson> reply,
    Pointer<BsonError> error);

/// returns bool
final writeCommandWithOptions = libmongoc.lookupFunction<
    MongoCReadCommandWithOptions,
    DartReadCommandWithOptions>('mongoc_database_write_command_with_opts');

typedef MongoCReadWriteCommandWithOptions = Int8 Function(
    Pointer<Void> db,
    Pointer<Bson> command,
    Pointer<Void> readPrefs,
    Pointer<Bson> options,
    Pointer<Bson> reply,
    Pointer<BsonError> error);

typedef DartReadWriteCommandWithOptions = int Function(
    Pointer<Void> db,
    Pointer<Bson> command,
    Pointer<Void> readPrefs,
    Pointer<Bson> options,
    Pointer<Bson> reply,
    Pointer<BsonError> error);

/// [readPres] will be ignored
/// returns bool
final readWriteCommandWithOptions = libmongoc.lookupFunction<
        MongoCReadWriteCommandWithOptions, DartReadWriteCommandWithOptions>(
    'mongoc_database_read_write_command_with_opts');

typedef MongoCCommandSimpe = Int8 Function(
    Pointer<Void> db,
    Pointer<Bson> command,
    Pointer<Void> readPrefs,
    Pointer<Bson> reply,
    Pointer<BsonError> error);

typedef DartCommandSimpe = int Function(Pointer<Void> db, Pointer<Bson> command,
    Pointer<Void> readPrefs, Pointer<Bson> reply, Pointer<BsonError> error);

/// [readPres] will be ignored
/// returns bool
final commandSimple = libmongoc.lookupFunction<
    MongoCReadWriteCommandWithOptions,
    DartReadWriteCommandWithOptions>('mongoc_database_command_simple');

final copy = libmongoc.lookupFunction<Pointer<Void> Function(Pointer<Void> db),
    Pointer<Void> Function(Pointer<Void> db)>('mongoc_database_copy');

typedef MongoCCreateCollection = Pointer<Void> Function(Pointer<Void> db,
    Pointer<Uint8> name, Pointer<Bson> options, Pointer<BsonError> error);
typedef DartCreateCollection = Pointer<Void> Function(Pointer<Void> db,
    Pointer<Uint8> name, Pointer<Bson> options, Pointer<BsonError> error);

// returns collection
final createCollection =
    libmongoc.lookupFunction<MongoCCreateCollection, DartCreateCollection>(
        'mongoc_database_create_collection');
